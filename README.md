#LabelView
<br />
android上用于显示标签云的组件
<br /><br />

功能：<br />
1、设置标签<br />
2、设置每个标签的配色方案<br />
3、设置每个标签的x/y速度<br />
4、设置标签云是否滚动(默认滚动)<br />
5、设置标签云的item点击事件<br />
<br />

具体使用方法：<br />

1、在xml中配置：<br />
&lt;org.loader.labelview.LabelView<p>
            xmlns:label="http://schemas.android.com/apk/res/org.loader.labelview"<p>
	        android:layout_marginTop="20dp"<p>
	        android:id="@+id/lv"<p>
	        android:layout_below="@id/et_input"<p>
	        android:layout_width="wrap_content"<p>
	        android:layout_height="wrap_content"<p>
	        label:is_static="false"<p>
	        android:background="@android:color/white"/>
            
在Activity中配置：<br />

    @Override
    protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		
		mEditText = (EditText) findViewById(R.id.et_input);
		mLabelView = (LabelView) findViewById(R.id.lv);
		
		mLabelView.setLabels(new String[] {"蛋疼","loader","Android", "Google", "馒头", "大米", "服务"});
		mLabelView.setColorSchema(new int[] {Color.DKGRAY, Color.CYAN, Color.GREEN, Color.LTGRAY, Color.MAGENTA, Color.RED});
		mLabelView.setSpeeds(new int[][] {{1,2},{1,1},{2,1},{2,3}});
		mLabelView.setOnItemClickListener(new OnItemClickListener() {
			@Override
			public void onItemClick(int index, String label) {
				Toast.makeText(MainActivity.this, "index : " + index + ",label : " + label, Toast.LENGTH_SHORT).show();
				mEditText.setText(label);
			}
		});
	}
    
效果展示（动态图有失真现象，所以直接截图了）：<br />
![image](http://git.oschina.net/qibin/LabelView/raw/master/images/1.png)
![image](http://git.oschina.net/qibin/LabelView/raw/master/images/2.png)