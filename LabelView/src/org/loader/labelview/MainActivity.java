package org.loader.labelview;

import org.loader.labelview.LabelView.OnItemClickListener;

import android.app.Activity;
import android.graphics.Color;
import android.os.Bundle;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends Activity {
	private EditText mEditText;
	private LabelView mLabelView;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		
		mEditText = (EditText) findViewById(R.id.et_input);
		mLabelView = (LabelView) findViewById(R.id.lv);
		
		mLabelView.setLabels(new String[] {"文件","编辑","Android", "Google", "馒头", "大米", "服务"});
		mLabelView.setColorSchema(new int[] {Color.DKGRAY, Color.CYAN, Color.GREEN, Color.LTGRAY, Color.MAGENTA, Color.RED});
		mLabelView.setSpeeds(new int[][] {{1,2},{1,1},{2,1},{2,3}});
		mLabelView.setOnItemClickListener(new OnItemClickListener() {
			@Override
			public void onItemClick(int index, String label) {
				Toast.makeText(MainActivity.this, "index : " + index + ",label : " + label, Toast.LENGTH_SHORT).show();
				mEditText.setText(label);
			}
		});
	}
}
